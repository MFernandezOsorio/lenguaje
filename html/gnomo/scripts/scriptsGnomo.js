/* CAMBIAR IMAGEN 1 */

function cambiar1()
{
	var imagenes = ["imagenes/berenjena.png", "imagenes/diamante.jpg", "imagenes/limon.png"],
		aleatorio = Math.floor(Math.random()*3), /* 3 ALEATORIOS PORQUE HAY 3 IMAGENES */
		imagenfinal1 = imagenes[aleatorio],
		tactualfinal = 0

		imagen1 = document.getElementById("1")
		imagen2 = document.getElementById("2")
		imagen3 = document.getElementById("3")
		tactual = document.getElementById("tiradaActual")

	imagen1.src = imagenfinal1 /* EXPLICO EL SIGUIENTE BUCLE MAS ABAJO */

	if( imagen1.src == imagen2.src && imagen1.src == imagen3.src && imagen2.src == imagen3.src )
		tactualfinal = 1000
	else
		if( imagen1.src == imagen2.src || imagen1.src == imagen3.src || imagen2.src == imagen3.src)
			tactualfinal = 500
		else
			tactualfinal = 0

	tactual.innerHTML = tactualfinal
}

/* CAMBIAR IMAGEN 2 */

function cambiar2()
{
	var imagenes = ["imagenes/berenjena.png", "imagenes/diamante.jpg", "imagenes/limon.png"],
		aleatorio = Math.floor(Math.random()*3),
		imagenfinal2 = imagenes[aleatorio],
		tactualfinal = 0

		imagen1 = document.getElementById("1")
		imagen2 = document.getElementById("2")
		imagen3 = document.getElementById("3")
		tactual = document.getElementById("tiradaActual")

	imagen2.src = imagenfinal2 /* EXPLICO EL SIGUIENTE BUCLE MAS ABAJO */

	if( imagen1.src == imagen2.src && imagen1.src == imagen3.src && imagen2.src == imagen3.src )
		tactualfinal = 1000
	else
		if( imagen1.src == imagen2.src || imagen1.src == imagen3.src || imagen2.src == imagen3.src)
			tactualfinal = 500
		else
			tactualfinal = 0

	tactual.innerHTML = tactualfinal
}

/* CAMBIAR IMAGEN 3 */

function cambiar3()
{
	var imagenes = ["imagenes/berenjena.png", "imagenes/diamante.jpg", "imagenes/limon.png"],
		aleatorio = Math.floor(Math.random()*3),
		imagenfinal3 = imagenes[aleatorio],
		tactualfinal = 0

		imagen1 = document.getElementById("1")
		imagen2 = document.getElementById("2")
		imagen3 = document.getElementById("3")
		tactual = document.getElementById("tiradaActual")

	imagen3.src = imagenfinal3 /* EXPLICO EL SIGUIENTE BUBLE EN LA SIGUIENTE FUNCION */

	if( imagen1.src == imagen2.src && imagen1.src == imagen3.src && imagen2.src == imagen3.src )
		tactualfinal = 1000
	else
		if( imagen1.src == imagen2.src || imagen1.src == imagen3.src || imagen2.src == imagen3.src)
			tactualfinal = 500
		else
			tactualfinal = 0

	tactual.innerHTML = tactualfinal
}

/* JUGAR */

function jugar()
{
	var imagenes = ["imagenes/berenjena.png", "imagenes/diamante.jpg", "imagenes/limon.png"],
		aleatorio1 = Math.floor(Math.random()*3), /* FLOOR PARA QUE SEAN ENTEROS, RANDOM GENERA ALEATORIO */
		aleatorio2 = Math.floor(Math.random()*3), /* EL 3 INDICA CUANTOS ALEATORIOS */
		aleatorio3 = Math.floor(Math.random()*3),
		imagenfinal1 = imagenes[aleatorio1],
		imagenfinal2 = imagenes[aleatorio2],
		imagenfinal3 = imagenes[aleatorio3],
		tactualfinal = 0
		imagen1 = document.getElementById("1")
		imagen2 = document.getElementById("2")
		imagen3 = document.getElementById("3")
		tactual = document.getElementById("tiradaActual")

	//imagen1.src=imagenfinal1
	//imagen2.src=imagenfinal2	/* TAMBIEN SE PODRIA PONER IMAGENFINALX DENTRO DEL IF */
	//imagen3.src=imagenfinal3

	if( imagen1.src == imagen2.src && imagen1.src == imagen3.src && imagen2.src == imagen3.src )
		tactualfinal = 1000 /* SI COINCIDEN 3 IMAGENER 1000 PUNTOS */
	else
		if( imagen1.src == imagen2.src || imagen1.src == imagen3.src || imagen2.src == imagen3.src)
			tactualfinal = 500 /* SI COINCIDEN 2 IMAGENER 500 PUNTOS */
		else
			tactualfinal = 0

	tactual.innerHTML = tactualfinal
}

/* GUARDAR TIRADA */

function guardar()
{
	var tactualfinal,
	mactualfinal

	mactual = document.getElementById("marcadorActual")
	tactual = document.getElementById("tiradaActual")

	tactualfinal=parseInt(tactual.textContent) /* SE CONVIERTE PARA PODER SUMARLOS COMO ENTEROS */
	mactualfinal=parseInt(mactual.textContent)

	mactualfinal += tactualfinal
	tactualfinal = 0


	mactual.textContent = mactualfinal
	tactual.textContent = tactualfinal
}

/* REINICIAR JUEGO */

function reiniciar(){
	var tactualfinal,
		mactualfinal

	imagen1 = document.getElementById("1")
	imagen2 = document.getElementById("2")
	imagen3 = document.getElementById("3")
	mactual = document.getElementById("marcadorActual")
	tactual = document.getElementById("tiradaActual")

	mactualfinal = 0 /* SE REINICIAN MARCADORES E IMAGENES */
	tactualfinal = 0

	imagen1.src = "imagenes/diamante.jpg"
	imagen2.src = "imagenes/limon.png"
	imagen3.src = "imagenes/berenjena.png"

	mactual.innerHTML = mactualfinal
	tactual.innerHTML = tactualfinal
}

/* OBTENER FECHA ACTUAL */

function fecha()
{
var fecha = new Date();
document.write(fecha.getDate() + " / " + (fecha.getMonth() +1) + " / " + fecha.getFullYear());
}