function UltimoDia(){
	var dia,
	mes,
	anio,
	dmax,
	resultado,
	dmes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

	dia = parseInt(document.getElementById("dia").value)
	mes = parseInt(document.getElementById("mes").value)
	anio = parseInt(document.getElementById("anio").value)
	resultado = document.getElementById("resultado")


	if ((anio%4) == 0) 
        dmes[1]=29
    else
        dmes[1]=28

    if (dia<=dmes[mes - 1] && mes <= 12)
    	dmax = dmes[mes - 1]
    else 
    	dmax = "Lo siento, fecha errónea"

	resultado.innerHTML = "El último día del mes " + mes + " es: " + dmax
}