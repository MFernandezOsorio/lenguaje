function VariablesNumericas()
{
	var parrafoVariable,
		n1 = 1,
		n2 = 3,
		n3 = 4

	parrafoVariable = document.getElementById('resultadoNumericas')
	parrafoVariable.innerHTML = "La suma es: " + (n1 + n2 + n3) + "<br>"
	parrafoVariable.innerHTML += "La media es: " + ((n1 + n2 + n3)/3) + "<br>"
}
function VariablesCadena()
{
	var parrafoVariable,
		c1 = "Hola"
		c2 = "José Luis"

	parrafoVariable = document.getElementById('resultadoCadena')
	parrafoVariable.innerHTML = "Las cadenas concatenadas con un guión: " + (c1 + "-" + c2) + "<br"
}
function VariablesLogicas()
{
	var parrafoVariable,
		boolean = !false

	parrafoVariable = document.getElementById('resultadoLogicas')
	parrafoVariable.innerHTML = "El valor de la variable es: " + (boolean) + "<br>"
}